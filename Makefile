
clean:
	mvn clean

package:
	$(MAKE) clean
	mvn package

twitter-model:
	mkdir -p data/twitter_model
	wget https://s3.amazonaws.com/auxdata.johnsnowlabs.com/public/models/analyze_sentimentdl_use_twitter_en_2.7.1_2.4_1610993470852.zip
	mv analyze_sentimentdl_use_twitter_en_2.7.1_2.4_1610993470852.zip data/twitter_model
	cd data/twitter_model; unzip analyze_sentimentdl_use_twitter_en_2.7.1_2.4_1610993470852.zip
