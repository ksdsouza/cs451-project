import requests
import csv

bearer = "" #put your bearer here

step = 5

day = 8
isAM = False
suffix = 0 if isAM else 1

with open(f"out-2021-04-{day:02}-part-{suffix}.csv", 'w', newline='') as csvfile:
    hour = 0 if isAM else 12
    next_hour = 0
    minute = 0
    next_minute = 0

    writer = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_NONNUMERIC)

    writer.writerow(['ID', 'created_at', 'text', 'like_count', 'rt_count'])

    while True:
        next_minute = (minute + step) % 60
        next_hour = (hour + 1) % 24 if next_minute == 0 else hour

        # this will break if you run this on April 30 but school is done then so that's ok!
        next_day = (day + 1) % 31 if hour > next_hour else day

        response = requests.get("https://api.twitter.com/2/tweets/search/recent",
                                headers={"Authorization": "Bearer " + bearer},
                                params={"query": "(ETH OR etherium) lang:en -is:retweet -is:quote -is:reply",
                                        "max_results": 100,
                                        "tweet.fields": "text,public_metrics,created_at",
                                        "start_time": f"2021-04-{day:02}T{hour:02}:{minute:02}:00Z",
                                        "end_time": f"2021-04-{next_day:02}T{next_hour:02}:{next_minute:02}:00Z"})

        if response.status_code == 200:
            count = response.json()['meta']['result_count']
            print(f"{hour}:{minute} {count}")
            if count > 0:
                for data in response.json()['data']:
                    writer.writerow([data['id'], data['created_at'], data['text'].replace('\n', ' ').replace('\r', ''), data['public_metrics']['like_count'], data['public_metrics']['retweet_count']])

            minute = next_minute
            hour = next_hour

            if isAM and hour == 12:
                break
            if next_day > day:
                break
        else:
            print(response.json())
            break
