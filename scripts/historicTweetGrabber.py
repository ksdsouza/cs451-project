from datetime import datetime, timedelta

import requests
import csv
import time

bearer = ""  # put your bearer here
env = "" # put your dev env here

step = timedelta(minutes=30)
currentDateStart = datetime(2021, 3, 30, hour=3, minute=30)
currentDateEnd = currentDateStart + step
endDate = datetime(2021, 4, 3)

while endDate - currentDateEnd > timedelta(0):
    time.sleep(4)
    currentDateStartStr = currentDateStart.strftime("%Y%m%d%H%M")
    currentDateEndStr = currentDateEnd.strftime("%Y%m%d%H%M")

    with open(f"out-{currentDateStartStr}.csv", 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(['ID', 'created_at', 'text', 'like_count', 'rt_count'])
        response = requests.post(
            f"https://api.twitter.com/1.1/tweets/search/30day/{env}.json",
            headers={"Authorization": f"Bearer {bearer}"},
            json={
                "query": "(ETH OR etherium) lang:en",
                "maxResults": 100,
                "fromDate": currentDateStartStr,
                "toDate": currentDateEndStr
            }
        )
        if response.status_code != 200:
            print(f"{currentDateStartStr}-{currentDateEndStr} failed - {response.text}")
            exit(1)

        responseBody = response.json()
        count = len(responseBody['results'])
        print(f"{currentDateStartStr}-{currentDateEndStr} count={count}")
        for data in responseBody['results']:
            tweetText = data['text']

            if 'extended_tweet' in data:
                tweetText = data['extended_tweet']['full_text']

            favouriteCount: int
            retweetCount: int
            if 'retweeted_status' in data:
                favouriteCount = data['retweeted_status']['favorite_count']
                if 'extended_tweet' in data['retweeted_status']:
                    tweetText = data['retweeted_status']['extended_tweet']['full_text']
            else:
                favouriteCount = data['favorite_count']
                retweetCount = data['retweet_count']

            tweetText = tweetText.replace('\n', ' ').replace('\r', '').replace('"', '')

            raw_created_at = data['created_at']

            created_timestamp = datetime.strptime(raw_created_at, '%a %b %d %H:%M:%S %z %Y').isoformat().replace("+00:00", "Z")

            writer.writerow([
                data['id'],
                created_timestamp,
                tweetText,
                favouriteCount,
                retweetCount,
            ])

    currentDateStart += step
    currentDateEnd += step
