package ca.uwaterloo.cs451

import org.apache.spark.sql.SparkSession

object NumTransactionsReader {
  val path = "data/export-TxGrowth.csv"

  def readCSV()(implicit spark: SparkSession): collection.Map[Long, Long] = {
    spark.read
      .option("header", "true")
      .csv(path)
      .drop("Date(UTC)")
      .rdd
      .map(row => {
        val unixTimestamp = row(0).toString.toLong * 1000
        val value = row(1).toString
        unixTimestamp -> value.toLong
      })
      .collectAsMap()
  }

  def getNumTransactionsForTimestamp(
                                      timestampToNumTransactions: collection.Map[Long, Long],
                                      timestampMs: Long*
                                    ): Seq[Long] = {
    timestampMs.map(timestamp => timestampToNumTransactions.getOrElse(timestamp, 0L))
  }
}