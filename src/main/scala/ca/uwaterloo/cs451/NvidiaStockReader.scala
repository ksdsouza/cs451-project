package ca.uwaterloo.cs451

import org.apache.spark.mllib.rdd.RDDFunctions.fromRDD
import org.apache.spark.sql.SparkSession

import java.text.SimpleDateFormat
import scala.collection.immutable.TreeMap

object NvidiaStockReader {
  val path = "data/NVDA-30-MIN-2021-04-15.csv"
  val path2 = "data/NVDA.csv"

  def readCSV()(implicit spark: SparkSession): TreeMap[Long, List[Double]] = {
    val map = spark.read
      .option("header", "true")
      .csv(path)
      .drop("Open", "High", "Low", "Volume")
      .rdd
      .map(row => {
        val dateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm a")

        val dateStr = row.getString(0)
        val timeStr = row.getString(1)
        val date = dateFormat.parse(f"$dateStr $timeStr").getTime

        (date, row.getString(2).toDouble)
      })
      .sortByKey()
      .sliding(26)
      .map(rows => {
        val timestampEnd = rows.last._1
        timestampEnd -> rows.map(_._2).toList
      })
      .collectAsMap()
      .toMap
    TreeMap[Long, List[Double]](map.toArray:_*)
  }
}
