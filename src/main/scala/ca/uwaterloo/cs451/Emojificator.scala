/**
 * Bespin: reference implementations of "big data" algorithms
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.uwaterloo.cs451


import com.johnsnowlabs.nlp.pretrained.PretrainedPipeline
import org.apache.log4j._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}
import org.rogach.scallop._
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat

import scala.util.Try


class EmojiConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, output)
  val input = opt[String](descr = "input path", required = true)
  val output = opt[String](descr = "output path", required = true)
  verify()
}


object Emojificator {
  val log = Logger.getLogger(getClass.getName)

  def main(argv: Array[String]) {
    val args = new EmojiConf(argv)

    val conf = new SparkConf().setAppName("Emojificator")
    val sc = new SparkContext(conf)
    implicit val spark: SparkSession = SparkSession.builder().getOrCreate()

    var result = 0L

    val emojiMap = sc.textFile("data/Emoji_Sentiment_Data_v1.0.csv")
      .map(x => x.split(','))
      .filter(x => x(0) != "Emoji")
      .map(x => (x(0), (x(6).toInt / x(2).toDouble, x(4).toInt / x(2).toDouble))) // %pos, %neg
      .collectAsMap.toMap

    val emojis = sc.broadcast(emojiMap)

    val tweetsRDD = spark.read.format("org.apache.spark.csv").option("header", "true").option("quote", "\"").option("escape", "\"").csv(args.input())
      .rdd
      .map(x => (x.getString(0), x.getString(1), x.getString(2), x.getString(3).toInt, x.getString(4).toInt))
      .map({ case (id, createdAt, text, likeCount, rtCount) =>
        val charPairs = text.sliding(2).toList // emojis are at most 2 chars long

        val sentiment = charPairs.map(substr => {
          val twoPartEmoji = emojis.value.get(substr)
          val onePartEmoji = emojis.value.get(substr.slice(0, 1))

          (twoPartEmoji, onePartEmoji) match {
            case (Some((twopartpos, twopartneg)), Some((onepartpos, onepartneg))) =>
              ((twopartpos + onepartpos) - (twopartneg + onepartneg), 2)
            case (None, Some((onepartpos, onepartneg))) => (onepartpos - onepartneg, 1)
            case (Some((twopartpos, twopartneg)), None) => (twopartpos - twopartneg, 1)
            case (None, None) => (0d, 0)
          }
        })
          .reduce((x: (Double, Int), y: (Double, Int)) => (x._1 + y._1, x._2 + y._2)) //sentiment, count

        val emojiSentiment = if (sentiment._2 == 0) 0d else sentiment._1 / sentiment._2
        Row(id, createdAt, text, likeCount, rtCount, emojiSentiment)
      })
      .persist()

    val pipeline = PretrainedPipeline.fromDisk("data/twitter_model")

    val schema = new StructType()
      .add("id", StringType, nullable = false)
      .add("time", StringType, nullable = false)
      .add("tweet", StringType, nullable = false)
      .add("likes", IntegerType, nullable = false)
      .add("rts", IntegerType, nullable = false)
      .add("emoji_sentiment", DoubleType, nullable = false)

    val tweetsDF = spark.createDataFrame(tweetsRDD, schema).cache()
    val twitterSentimentMap = pipeline.annotate(tweetsDF, "tweet")
      .select("id", "time", "text", "likes", "rts", "emoji_sentiment", "sentiment")
      .rdd
      .map(row => {
        val id = row.getString(0)
        val time = row.getString(1)
        val text = row.getString(2)
        val likes = row.getInt(3)
        val rts = row.getInt(4)
        val emojiSentiment = row.getDouble(5)
        val sentimentArray = row.getList[Row](6).get(0)

        val positivityMap = sentimentArray.getMap[String, String](4)
        val sentiment = sentimentArray.getString(3)
        val positiveSentimentScore = positivityMap("positive").toDouble
        val negativeSentimentScore = positivityMap("negative").toDouble
        val timebyhour = time.slice(0,13)
        val positiveSentimentCount = if (sentiment == "positive") 1 else 0

        (timebyhour, (emojiSentiment, likes * positiveSentimentScore, rts * positiveSentimentScore,
        likes * negativeSentimentScore, rts * positiveSentimentScore, positiveSentimentScore, negativeSentimentScore, 
        positiveSentimentCount, 1 - positiveSentimentCount,
        1))
      })
      .reduceByKey((x,y) => (x._1 + y._1,x._2 + y._2,x._3 + y._3,x._4 + y._4,x._5 + y._5,x._6 + y._6,x._7 + y._7,x._8 + y._8,x._9 + y._9,x._10 + y._10))
      .map ({case ((timebyhour, (emojiSentiment, poslikes, posrts, neglikes, negrts, posscore, negscore, poscount, negcount, count))) =>
        (timebyhour, List(emojiSentiment/count, poslikes/count, posrts/count, neglikes/count, negrts/count, posscore/count, negscore/count, poscount.toDouble/count, negcount.toDouble/count, poscount.toDouble, negcount.toDouble, count.toDouble))
      })
      .collectAsMap.toMap

    val twitterMap = sc.broadcast(twitterSentimentMap)

    val fullfeatures = spark.read.format("org.apache.spark.csv").option("header", "true").csv("./data/gemini_ETHUSD_1hr.csv")
      .rdd
      .map(x => (x.getString(1), List(x.getString(3).toDouble, x.getString(4).toDouble,x.getString(5).toDouble,x.getString(6).toDouble,x.getString(7).toDouble)))
      .map (x => {
        (x._1.slice(0,13).replace(' ','T'), x._2) // make the datetime resemble the one from twitter and use that as key
      })
      .flatMap(x => {
        val key = x._1
        val value = x._2
        val dt = ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC).parseDateTime(key+":00:00.000Z").getMillis()
        if (twitterMap.value.contains(key)) {
          List((dt, value ++ twitterMap.value(key)))
        } else {
          None
        }
      })
      .map(x => x._1+",\"" + x._2 + "\"")
      .saveAsTextFile(args.output())
  }
}