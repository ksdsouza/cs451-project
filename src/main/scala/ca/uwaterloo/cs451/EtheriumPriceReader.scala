package ca.uwaterloo.cs451

import org.apache.spark.sql.SparkSession

class EtheriumPriceReader(implicit spark: SparkSession) {
  val hourlyData = spark.read
    .option("header", "true")
    .format("csv")
    .load("./data/gemini_ETHUSD_1hr.csv")
    .drop("Date", "Symbol", "High", "Low", "Volume")

}
