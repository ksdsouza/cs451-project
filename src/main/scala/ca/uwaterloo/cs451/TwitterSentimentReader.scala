package ca.uwaterloo.cs451

import org.apache.spark.sql.SparkSession

object TwitterSentimentReader {
  val path = "data/twitter_sentiment.csv"

  def readCSV()(implicit spark: SparkSession): collection.Map[Long, List[Double]] = {
    spark.read
      .option("header", "true")
      .csv(path)
      .rdd
      .map(row => {
        val unixTimestamp = row(0).toString.toLong // csv timestamp is in epoch ms already
        val value = row(1).toString.replace("List("," ").replace(")"," ").split(",").toList.map(x=>x.toDouble)  // row(1) is a list!
        unixTimestamp -> value
      })
      .collectAsMap()
  }

  def getSentimentsForTimestamp(timestampToSentiment: collection.Map[Long, List[Double]], timestampMs: Long*): Seq[List[Double]] = {
    timestampMs.map(timestamp => timestampToSentiment.getOrElse(timestamp, List[Double](2071.1, 2085.7, 2071.04, 2081.84, 695.72409967, 0.15599661635213202, 4.006389368852438, 1.2233610098900372, 0.3565735953385984, 1.2233610098900372, 0.8948098063676135, 0.10519019376012582, 0.8907407407407407, 0.10925925925925926, 481.0, 59.0, 540.0)))
  }
}
