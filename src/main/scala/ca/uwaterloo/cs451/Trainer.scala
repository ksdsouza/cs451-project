package ca.uwaterloo.cs451

import ca.uwaterloo.cs451.BlockDifficultyReader.getDifficultyForTimestamps
import ca.uwaterloo.cs451.NetworkHashReader.getHashesForTimestamps
import ca.uwaterloo.cs451.NumTransactionsReader.getNumTransactionsForTimestamp
import ca.uwaterloo.cs451.TwitterSentimentReader.getSentimentsForTimestamp
import org.apache.spark.ml.classification.{RandomForestClassificationModel, RandomForestClassifier}
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.linalg.DenseVector
import org.apache.spark.mllib.rdd.RDDFunctions._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{asc, desc}
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable


object Trainer {
  val conf = new SparkConf().setAppName(getClass.getName).setMaster("local")
  val sc = new SparkContext(conf)
  implicit val spark: SparkSession = SparkSession.builder().getOrCreate()

  import spark.implicits._

  val ONE_DAY_IN_MS = 1 * 24 * 60 * 60 * 1000
  val FIFTEEN_MINS_IN_MS = 15 * 60 * 1000
  val THIRTY_MINS_IN_MS = 30 * 60 * 1000
  val LABEL_DOWN = 0d
  val LABEL_UP = 1d

  case class ModelBreakdown
  (
    totalUp: Int,
    guessedUp: Int,
    correctUp: Int,
    totalDown: Int,
    guessedDown: Int,
    correctDown: Int,
  )

  def main(args: Array[String]): Unit = {
    val timestampToHashRate = sc.broadcast(NetworkHashReader.readCSV())
    val timestampToTxVolume = sc.broadcast(NumTransactionsReader.readCSV())
    val timestampToDifficulty = sc.broadcast(BlockDifficultyReader.readCSV())
    val timestampToSentiment = sc.broadcast(TwitterSentimentReader.readCSV())
    val timestampToNvidiaStock = sc.broadcast(NvidiaStockReader.readCSV())
    val timestampToAMDStock = sc.broadcast(AMDStockReader.readCSV())

    val data = new EtheriumPriceReader()
      .hourlyData
      .rdd
      .map(row => {
        val timestamp = row(0).toString.padTo(13, '0').toLong
        val openPrice = row(1).toString.toDouble
        val closePrice = row(2).toString.toDouble
        (timestamp, (openPrice, closePrice))
      })
      .sortByKey()
      .sliding(49) // Most recent hour window for the label, previous 48 1h windows for the features
      .map(window => {
        val (predictionTimestamp, (actualOpen, actualClose)) = window.last
        val trainingRows = window.dropRight(1)

        val label = if (actualOpen > actualClose) LABEL_DOWN else LABEL_UP
        (predictionTimestamp, (trainingRows.toSeq, label))
      })
      .filter({ case (predictionTimestamp, _) =>
        predictionTimestamp >= 1616544000000L // march 24
      })
      .map({ case (predictionTimestamp, (ethPriceData, label)) =>
        val timestampRounded1 = predictionTimestamp - (predictionTimestamp % ONE_DAY_IN_MS) - ONE_DAY_IN_MS // 1 day from prediction window
        val timestampRounded2 = timestampRounded1 - ONE_DAY_IN_MS // 2 days from prediction timestamp

        val hashRates = getHashesForTimestamps(timestampToHashRate.value, timestampRounded1, timestampRounded2)
        val numTransactions = getNumTransactionsForTimestamp(timestampToTxVolume.value, timestampRounded1, timestampRounded2)
        val blockDifficulties = getDifficultyForTimestamps(timestampToDifficulty.value, timestampRounded1, timestampRounded2)
        val tweetSentiments = getSentimentsForTimestamp(timestampToSentiment.value, timestampRounded1, timestampRounded2)

        val closingPrices = ethPriceData.map({ case (_, (_, closePrice)) => closePrice })
        val nvidiaStock = timestampToNvidiaStock.value.to(predictionTimestamp - THIRTY_MINS_IN_MS).head._2
        val amdStock = timestampToAMDStock.value.to(predictionTimestamp - THIRTY_MINS_IN_MS).head._2

        val features = (
          closingPrices.toList.reverse
            ++ hashRates.toList
            ++ numTransactions.toList
            ++ blockDifficulties.toList
            ++ tweetSentiments.toList.flatten
            ++ nvidiaStock
            ++ amdStock
          )
          .map(_.toString.toDouble)
          .toArray
        val vector = new DenseVector(features)
        (predictionTimestamp, label, vector)
      })
      .toDF("timestamp", "label", "features")

    val total = data.count().toInt

    val trainingCount = Math.floor(total * .8).toInt

    val training = data.limit(trainingCount)
    val testing = data
      .orderBy(desc("timestamp"))
      .limit(total - trainingCount)
      .orderBy(asc("timestamp"))

    var bestProfitTreeSeed = 1
    var bestProfitAccuracy = 0d
    var bestProfitProfit = Double.NegativeInfinity
    var bestAccuracyTreeSeed = 1
    var bestAccuracyAccuracy = 0d
    var bestAccuracyProfit = Double.NegativeInfinity
    var totalProfit = 0d
    for {i <- 1 to 1000} yield {
      val rf = new RandomForestClassifier()
        .setLabelCol("label")
        .setFeaturesCol("features")
        .setNumTrees(10)
        .setSeed(i)

      val model: RandomForestClassificationModel = rf.fit(training)
      val predictions = model.transform(testing)

      val ModelBreakdown(totalUp, guessedUp, correctUp, totalDown, guessedDown, correctDown) = getModelBreakdown(
        model, testing.rdd.toLocalIterator.map(row => (row.getDouble(1), row.getAs[DenseVector](2)))
      )

      val evaluator = new MulticlassClassificationEvaluator()
        .setLabelCol("label")
        .setPredictionCol("prediction")
        .setMetricName("accuracy")
      val accuracy = evaluator.evaluate(predictions)

      var initialEthPrice: Double = Double.NaN
      var lastEthPrice = 0d;
      var initialEth = 0d;
      val netHistory = mutable.MutableList.empty[(Long, Double, Double)]
      val (finalEth, finalDollars) = testing.rdd.toLocalIterator.foldLeft((0d, 10000d))({
        case ((eth, dollars), row) =>
          val timestamp = row.getLong(0)
          val features = row.getAs[DenseVector](2)
          val prediction = model.predict(features)
          val ethPrice = features(0)
          lastEthPrice = ethPrice
          if (initialEthPrice.isNaN) {
            initialEthPrice = ethPrice
            initialEth = dollars / ethPrice
          }
          var ret = (0d, 0d)
          val netWorth = dollars + ethPrice * eth
//          netHistory += Tuple3(timestamp, netWorth, initialEth * ethPrice)
          if (prediction == LABEL_DOWN) {
            if (eth < 0) {
              ret = (eth, dollars)
            } else {
              val short = (dollars + (eth * ethPrice)) / ethPrice
              ret = (-short, 2 * (dollars + (eth * ethPrice)))
            }
          } else {
            if (eth < 0) {
              val netDollars = dollars + eth * ethPrice
              if (netDollars <= 0) {
                ret = (0d, 0d)
              } else {
                ret = (netDollars / ethPrice, 0d)
              }
            } else if (dollars == 0) {
              ret = (eth, dollars)
            } else {
              ret = (dollars / ethPrice, 0d)
            }
          }
          ret
      })

      val buyAndHoldProfit = 10000 * (lastEthPrice - initialEthPrice) / initialEthPrice
      val finalProfit = (finalDollars + finalEth * lastEthPrice) - 10000.0

      totalProfit += finalProfit

      println(f"${("000" + i) takeRight 4}: Test accuracy = $accuracy trainingSize=${training.count()} testingSize=${testing.collect().length}")
      println(f"${("000" + i) takeRight 4}: Total Up=$totalUp GuessedUp=$guessedUp correctUp=$correctUp")
      println(f"${("000" + i) takeRight 4}: Total Down=$totalDown GuessedDown=$guessedDown correctDown=$correctDown")
      println(f"${("000" + i) takeRight 4}: Final Profit: ₽$finalProfit, Average Profit So Far: ₽${totalProfit / i}, Buy And Hold Profit: $buyAndHoldProfit")
//      netHistory.foreach(println)
      if (bestProfitProfit < finalProfit) {
        bestProfitTreeSeed = i
        bestProfitAccuracy = accuracy
        bestProfitProfit = finalProfit
      }
      if (bestAccuracyAccuracy < accuracy) {
        bestAccuracyTreeSeed = i
        bestAccuracyAccuracy = accuracy
        bestAccuracyProfit = finalProfit
      }
      println(f"${("000" + i) takeRight 4}: PROFIT\tSEED=$bestProfitTreeSeed\tACCURACY=$bestProfitAccuracy\tPROFIT=$bestProfitProfit")
      println(f"${("000" + i) takeRight 4}: ACCURACY\tSEED=$bestAccuracyTreeSeed\tACCURACY=$bestAccuracyAccuracy\tPROFIT=$bestAccuracyProfit")
    }

    println(f"PROFIT\tSEED=$bestProfitTreeSeed\tACCURACY=$bestProfitAccuracy\tPROFIT=$bestProfitProfit")
    println(f"ACCURACY\tSEED=$bestAccuracyTreeSeed\tACCURACY=$bestAccuracyAccuracy\tPROFIT=$bestAccuracyProfit")
  }

  def getModelBreakdown(model: RandomForestClassificationModel, testingData: Iterator[(Double, DenseVector)]): ModelBreakdown = {
    var guessedUp = 0
    var correctUp = 0
    var totalUp = 0

    var guessedDown = 0
    var correctDown = 0
    var totalDown = 0

    testingData.foreach({ case (actualLabel, features) =>
      val predictedLabel = model.predict(features)
      totalUp += (if (actualLabel == LABEL_UP) 1 else 0)
      totalDown += (if (actualLabel == LABEL_DOWN) 1 else 0)

      if (predictedLabel == LABEL_UP) {
        guessedUp += 1
        if (predictedLabel == actualLabel) {
          correctUp += 1
        }
      }
      else {
        guessedDown += 1
        if (predictedLabel == actualLabel) {
          correctDown += 1
        }
      }
    })
    ModelBreakdown(totalUp, guessedUp, correctUp, totalDown, guessedDown, correctDown)
  }

}
