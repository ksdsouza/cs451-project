package ca.uwaterloo.cs451

import com.johnsnowlabs.nlp.pretrained.PretrainedPipeline
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}

object main {


  val conf = new SparkConf().setAppName(getClass.getName)
  val sc = new SparkContext(conf)
  implicit val spark: SparkSession = SparkSession.builder().getOrCreate()

  def main(args: Array[String]): Unit = {
    val pipeline = PretrainedPipeline.fromDisk("data/twitter_model")

    val schema = new StructType()
      .add(StructField("tweet", StringType, nullable = false))
      .add(StructField("id", IntegerType, nullable = false))

    val tweetsRDD: RDD[Row] = sc.parallelize(List("ETH/BTC lookin good #Ethereum 🚀🚀🚀"))
      .map(tweet => Row(tweet, 0))
      .cache()

    val tweetsDF = spark.createDataFrame(tweetsRDD, schema)

    pipeline.annotate(tweetsDF, "tweet")
      .foreach(row => println(f"${row(0).toString}, ${row(1).toString}, ${row(2).toString}"))

  }

}
