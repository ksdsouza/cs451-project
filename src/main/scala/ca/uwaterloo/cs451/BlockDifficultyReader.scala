package ca.uwaterloo.cs451

import org.apache.spark.sql.SparkSession

object BlockDifficultyReader {
  val path = "data/export-BlockDifficulty.csv"

  def readCSV()(implicit spark: SparkSession) = {
    spark.read
      .option("header", "true")
      .csv(path)
      .drop("Date(UTC)")
      .rdd
      .map(row => {
        val unixTimestamp = row(0).toString.toLong * 1000
        val value = row(1).toString
        unixTimestamp -> value.toFloat
      })
      .collectAsMap()
  }

  def getDifficultyForTimestamps(timestampToDifficulty: collection.Map[Long, Float], timestampMs: Long*): Seq[Float] = {
    timestampMs.map(timestamp => timestampToDifficulty.getOrElse(timestamp, 0f))
  }
}