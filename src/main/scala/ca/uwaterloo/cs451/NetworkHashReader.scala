package ca.uwaterloo.cs451

import org.apache.spark.sql.SparkSession

object NetworkHashReader {
  val path = "data/export-NetworkHash.csv"

  def readCSV()(implicit spark: SparkSession): collection.Map[Long, Float] = {
    spark.read
      .option("header", "true")
      .csv(path)
      .drop("Date(UTC)")
      .rdd
      .map(row => {
        val unixTimestamp = row(0).toString.toLong * 1000 // csv timestamp is in epoch seconds
        val value = row(1).toString.toFloat
        unixTimestamp -> value
      })
      .collectAsMap()
  }

  def getHashesForTimestamps(timestampToHashRate: collection.Map[Long, Float], timestampMs: Long*): Seq[Float] = {
    timestampMs.map(timestamp => timestampToHashRate.getOrElse(timestamp, 0f))
  }
}
